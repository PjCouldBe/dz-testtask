**Digital Zone test task.**

This task is about to creating REST-service based on Java EE + Spring stack for inspection of website visits and calculation visit statistics. 
There is basic structure explanation, deploy,launch and test configuration and instructions.


_Project Structure_

The project is quite simple and consists from server and shared parts, and also the parent module.
Parent module is responsible for representing the service as one piece system and provides basic project settings.
Server nodule is core of the service contains controller endpoints, logic, interaction with DB and settings for it.
Shared module is responsible for model part that should be shared between server and clients (like statistics container object).



_Deploy instructions_
To deploy the project:

1. Download sources from repository using Git or plain downloading.
2. Build project using `mvn clean install` command.
3. Create the database manually on the server you specified using credentials defined in .env file. Use next commands:
    
    3.a. psql -U <rootUser> to enter the PostgreSQL command line interface.
    
    3.b. `create user <your_user> with password '<your_password>';` to create user.
    
    3.c. `create database testtask_db owner <your_user> encoding 'UTF8';` to create database and give all permissions on it to specified user
    
    3.d. Use `psql -U <your_user> -d testtask_db` to connect to database via command line interface.
    
You may also use the UI analogously (e.g. PgAdmin) for same operations.



_Launch and test instructions_

1. To launch the application open the command line and run: 
   `java -jar -DPOSTGRES_URL=<your_postgres_url> -DPOSTGRES_USER=<your_user> -DPOSTGRES_PASS=<your_pass> <path_to_target>/dz-testtask.jar`
2. You may also use:
   `java -jar <path_to_target>/dz-testtask.jar -Dspring.profiles.active=dev` to run the service with developer profile with predefined settings 
   (see application.yml file for details).

WARNING! First launch may take a long time (about a minute).