package com.dz.testtask.shared.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.annotation.Nullable;

/**
 * DTO for Visit Service client represents the statistics of visiting web pages
 * by different users for some period.
 *
 * Created by Dima on 16.08.2018.
 */
@ToString
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VisitStatistics {
    @Getter
    private long totalVisits;
    @Getter
    private long uniqueUsersAmount;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long permanentUserAmount;

    public VisitStatistics(long totalVisits, long uniqueUsersAmount) {
        this.totalVisits = totalVisits;
        this.uniqueUsersAmount = uniqueUsersAmount;
        this.permanentUserAmount = null;
    }

    @Nullable
    public Long getPermanentUserAmount() {
        return permanentUserAmount;
    }
}
