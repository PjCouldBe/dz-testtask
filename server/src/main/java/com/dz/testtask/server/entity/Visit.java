package com.dz.testtask.server.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Represent an event of visiting web page by some user
 *
 * Created by Dima on 16.08.2018.
 */
@Entity
@Table(name = "visit")
@Data
@EqualsAndHashCode(exclude = "id")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Visit {
    @Id
    @SequenceGenerator(name = "visit_id", sequenceName = "visit_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "visit_id")
    private Long id;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "page_id", nullable = false)
    private String pageId;

    @Column(name = "time", nullable = false)
    private Timestamp time;
}
