package com.dz.testtask.server.repository;

import com.dz.testtask.server.exception.VisitStatisticsException;
import com.dz.testtask.shared.dto.VisitStatistics;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

public class VisitRepositoryImpl implements VisitRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public VisitStatistics countStatisticsByTimeBetween(@Nonnull Timestamp begin, @Nonnull Timestamp end) throws VisitStatisticsException {
        final String SQL =
                "select " +
                    "sum(sub1.visits) AS totalVisits, " +
                    "count(sub1.user_id) AS uniqueUsersAmount, " +
                    "count(case when (sub1.uniqueVisits >= 10) then 1 else null end) AS permanentUserAmount " +
                "from " +
                    "(select user_id, count(page_id) AS visits, count(distinct page_id) as uniqueVisits " +
                    "from visit " +
                    "where visit.time >= :beginTime and visit.time <= :endTime " +
                    "group by user_id) " +
                    "AS sub1";
        List lst = em.createNativeQuery(SQL)
                .setParameter("beginTime", begin)
                .setParameter("endTime", end)
                .getResultList();

        return convertResultFromStatQuery(lst);
    }

    private VisitStatistics convertResultFromStatQuery(List result) throws VisitStatisticsException {
        final String ERR_MSG = "Error during statistics obtaining! Please contact support";

        if (result == null || result.size() <= 0 || result.size() > 1) {
            throw new VisitStatisticsException(ERR_MSG);
        }

        Object[] res0 = (Object[])result.get(0);
        if (res0.length != 3) throw new VisitStatisticsException(ERR_MSG);

        try {
            return new VisitStatistics(
                    ((BigDecimal)res0[0]).longValue(),
                    ((BigInteger)res0[1]).longValue(),
                    ((BigInteger)res0[2]).longValue()
            );
        } catch (RuntimeException e) {
            throw new VisitStatisticsException(ERR_MSG, e);
        }
    }
}
