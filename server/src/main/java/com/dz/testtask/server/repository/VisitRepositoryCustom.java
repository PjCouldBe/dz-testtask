package com.dz.testtask.server.repository;

import com.dz.testtask.server.exception.VisitStatisticsException;
import com.dz.testtask.shared.dto.VisitStatistics;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;

public interface VisitRepositoryCustom {
    VisitStatistics countStatisticsByTimeBetween(@Param("beginTime") Timestamp begin, @Param("endTime") Timestamp end)
            throws VisitStatisticsException;
}
