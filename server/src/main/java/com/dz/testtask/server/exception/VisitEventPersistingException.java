package com.dz.testtask.server.exception;

/**
 * Exception for situation when handler can not persist new visit event due to some
 * database exceptions or invalid input.
 *
 * Created by Dima on 16.08.2018.
 */
public class VisitEventPersistingException extends Exception {
    public VisitEventPersistingException(String message) {
        super(message);
    }

    public VisitEventPersistingException(String userId, String url, Throwable cause) {
        super(
                "There was critical error occurred while saving visit event with user: " + userId
                        + " and URL: " + url + " . Contact support or see verbose logs if you are developer.",
                cause
        );
    }

    public VisitEventPersistingException(String message, Throwable cause) {
        super(message, cause);
    }
}
