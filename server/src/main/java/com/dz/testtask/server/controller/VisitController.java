package com.dz.testtask.server.controller;

import com.dz.testtask.server.exception.VisitEventPersistingException;
import com.dz.testtask.server.exception.VisitStatisticsException;
import com.dz.testtask.server.service.VisitService;
import com.dz.testtask.shared.dto.VisitStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;

import static com.dz.testtask.server.utils.TimestampUtils.timestampOf;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Controller of endpoints list for storing new visit events and requesting period statistics (by test task)
 *
 * Created by Dima on 17.08.2018.
 */
@RestController
@RequestMapping(path = "/api/1/visits")
public class VisitController {
    private final VisitService visitService;

    @Autowired
    public VisitController(@Nonnull VisitService visitService) {
        this.visitService = visitService;
    }


    @RequestMapping(method = RequestMethod.POST, path = "/new")
    public ResponseEntity<VisitStatistics> saveNewVisit(@RequestParam(value = "userid") String userId,
                                                        @RequestParam(value = "pageid") String pageId)
            throws VisitEventPersistingException, VisitStatisticsException
    {
        if ( isNullOrEmpty(userId) ) {
            throw new VisitEventPersistingException("The User identifier was not specified!");
        }
        if ( isNullOrEmpty(pageId) ) {
            throw new VisitEventPersistingException("The Web-Page identifier was not specified!");
        }

        visitService.saveNewVisit(userId, pageId);

        return ResponseEntity.ok( visitService.getStatisticsForLastDay() );
    }



    @RequestMapping(method = RequestMethod.GET, path = "/stat")
    public ResponseEntity<VisitStatistics> getStatistics(@RequestParam(value = "begin_seconds") Long beginSeconds,
                                                         @RequestParam(value = "end_seconds") Long endSeconds)
            throws VisitStatisticsException
    {
        if (beginSeconds == null || beginSeconds < 0 || endSeconds == null || endSeconds < 0) {
            throw new VisitStatisticsException("Beginning or ending of the period was not specified " +
                    "or was specified incorrectly!");
        }
        if (beginSeconds > endSeconds) {
            throw new VisitStatisticsException("Beginning of the specified period must be less than its ending!");
        }

        return ResponseEntity.ok(
                visitService.getStaticsForPeriod(
                        timestampOf(beginSeconds),
                        timestampOf(endSeconds)
                )
        );
    }


    @ExceptionHandler(VisitEventPersistingException.class)
    public ResponseEntity<String> handleVisitEventPersistingException(VisitEventPersistingException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body( e.getLocalizedMessage() );
    }

    @ExceptionHandler(VisitStatisticsException.class)
    public ResponseEntity<String> handleVisitStatisticsException(VisitStatisticsException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body( e.getLocalizedMessage() );
    }
}
