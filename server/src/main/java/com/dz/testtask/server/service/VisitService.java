package com.dz.testtask.server.service;

import com.dz.testtask.server.entity.Visit;
import com.dz.testtask.server.exception.VisitEventPersistingException;
import com.dz.testtask.server.exception.VisitStatisticsException;
import com.dz.testtask.shared.dto.VisitStatistics;

import javax.annotation.Nonnull;
import java.sql.Timestamp;

/**
 * Facade for requests to visit repository for gathering some statistics or persisting new visit event.
 *
 * Created by Dima on 16.08.2018.
 */
public interface VisitService {
    @SuppressWarnings("UnusedReturnValue")
    @Nonnull
    Visit saveNewVisit(@Nonnull String userId, @Nonnull String url) throws VisitEventPersistingException;

    @Nonnull
    VisitStatistics getStatisticsForLastDay() throws VisitStatisticsException;

    @Nonnull
    VisitStatistics getStaticsForPeriod(@Nonnull Timestamp begin, @Nonnull Timestamp end) throws VisitStatisticsException;
}
