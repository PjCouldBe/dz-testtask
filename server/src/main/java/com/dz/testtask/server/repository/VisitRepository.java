package com.dz.testtask.server.repository;

import com.dz.testtask.server.entity.Visit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VisitRepository extends JpaRepository<Visit, Long>, VisitRepositoryCustom {
}
