package com.dz.testtask.server.exception;

/**
 * Excpetion for situations when handler cannot propose visit statistics due to some errors
 * or invalid input parameters.
 *
 * Created by Dima on 16.08.2018.
 */
public class VisitStatisticsException extends Exception {
    public VisitStatisticsException(String message) {
        super(message);
    }

    public VisitStatisticsException(String message, Throwable cause) {
        super(message, cause);
    }
}
