package com.dz.testtask.server.service;

import com.dz.testtask.server.entity.Visit;
import com.dz.testtask.server.exception.VisitEventPersistingException;
import com.dz.testtask.server.exception.VisitStatisticsException;
import com.dz.testtask.server.repository.VisitRepository;
import com.dz.testtask.shared.dto.VisitStatistics;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Calendar;

import static com.dz.testtask.server.utils.TimestampUtils.timestampOfInstant;
import static com.dz.testtask.server.utils.TimestampUtils.timestampOfNow;

/**
 * Implementation of VisitService based on simple operations to JDBC repositories.
 *
 * Created by Dima on 16.08.2018.
 */
@Service
@Slf4j
public class VisitServiceImpl implements VisitService {
    private final VisitRepository visitRepository;

    @Autowired
    public VisitServiceImpl(@Nonnull VisitRepository visitRepository) {
        this.visitRepository = visitRepository;
    }


    @Override
    @Nonnull
    @Transactional
    @Async
    public Visit saveNewVisit(@Nonnull String userId, @Nonnull String pageId) throws VisitEventPersistingException {
        try {
            String urlNormal = URLDecoder.decode(pageId, StandardCharsets.UTF_8.name());
            Timestamp time = timestampOfNow();

            Visit v = Visit.builder()
                    .userId(userId)
                    .pageId(urlNormal)
                    .time(time)
                    .build();
            v = visitRepository.save(v);
            log.debug("Saved new visit: id = {}, user = {}, pageId = {}, time = {}",
                    v.getId(), v.getUserId(), v.getPageId(), v.getTime());

            return v;
        } catch (UnsupportedEncodingException e) {
            log.error(e.getLocalizedMessage(), e);
            throw new VisitEventPersistingException(userId, pageId, e);
        }
    }



    @Override
    @Nonnull
    @Transactional
    public VisitStatistics getStatisticsForLastDay() throws VisitStatisticsException {
        Timestamp now = timestampOfNow();
        return getStaticsForPeriod( getBeginningOfDay(now), now, false);
    }

    @Override
    @Nonnull
    public VisitStatistics getStaticsForPeriod(@Nonnull Timestamp begin, @Nonnull Timestamp end) throws VisitStatisticsException {
        return getStaticsForPeriod(begin, end, true);
    }


    @Nonnull
    private VisitStatistics getStaticsForPeriod(@Nonnull Timestamp begin,
                                                @Nonnull Timestamp end,
                                                boolean withPermanents)
            throws VisitStatisticsException
    {
        VisitStatistics vs = visitRepository.countStatisticsByTimeBetween(begin, end);
        if ( ! withPermanents) {
            vs.setPermanentUserAmount(null);
        }

        return vs;
    }



    @Nonnull
    private Timestamp getBeginningOfDay(Timestamp momentOfDay) {
        Calendar c = Calendar.getInstance();
        c.setTime(momentOfDay);

        //Beginning of the current day is midnight: 00:00:00 moment of the day.
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        return timestampOfInstant(
                Instant.ofEpochMilli(c.getTimeInMillis())
        );
    }
}
