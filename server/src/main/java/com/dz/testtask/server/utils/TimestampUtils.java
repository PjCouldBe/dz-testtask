package com.dz.testtask.server.utils;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Utility class generating Timestamps in right timezone: UTC
 *
 * Created by Dima on 21.08.2018.
 */
public class TimestampUtils {
    public static Timestamp timestampOf(long seconds) {
        return Timestamp.valueOf(
                LocalDateTime.ofEpochSecond(seconds, 0, ZoneOffset.UTC)
        );
    }

    public static Timestamp timestampOfInstant(Instant instant) {
        return Timestamp.valueOf(
                LocalDateTime.ofInstant(instant, ZoneOffset.UTC)
        );
    }

    public static Timestamp timestampOfNow() {
        return timestampOfInstant( Instant.now() );
    }
}
